#! /bin/bash

SCRIPT=$(basename $0)
USAGE="
NAME
    ${SCRIPT} - setup pipeline structure

SYNOPSIS
    ${SCRIPT} samples

DESCRIPTION
    ${SCRIPT} sets up directory structure and makefile(s) for
    standard pipeline runs.

ARGUMENTS
    samples
        sample description file
"

################################################################################
# FUNCTIONS

function usage {
    echo "${USAGE}" >&2
}
function require_dir {
    mkdir -p $1 || log_fatal "Could not create directory $1"
}


################################################################################
# SCRIPT

# set pipeline and run directories
pipe_dir=$(cd $(dirname ${0}) && pwd)
run_dir=$(cd ${pipe_dir}/.. && pwd)

# load library and config settings
source ${pipe_dir}/lib/common.sh || log_fatal "Could not source ${pipe_dir}/lib/common.sh"
source ${pipe_dir}/lib/config.sh || log_fatal "Could not source ${pipe_dir}/lib/config.sh"

log_info "run dir:  ${run_dir}"
log_info "pipe dir: ${pipe_dir}"

# check for sample file
if [[ -z "$1" ]]; then
    usage
    log_fatal "Missing sample description file"
fi

[[ -f "$1" ]] && log_info "Sample file found: ${1}" \
              || log_fatal "Sample file not found: ${1}"


# set up and check the directory structure
[[ -d ${D_SEQ} ]] && log_info "Sequence directory found: ${D_SEQ}" \
                  || log_fatal "Sequence directory not found: ${D_SEQ}"

# set up the directory structure in the run directory
require_dir ${D_ALN}
require_dir ${D_QC}
require_dir ${D_BATCH}
require_dir ${D_TRACK}
require_dir ${D_OTHER}
require_dir ${D_TEMP}
require_dir ${D_ANNOT}

# check requirements from the configuration file
[[ -f "${CONTAMINANTS}" ]] && log_info  "Fastqc contaminant file found: ${CONTAMINANTS}" \
                         || log_fatal "Fastqc contaminant file not found: ${CONTAMINANTS}"
[[ -d "${D_BOWTIE_INDEX}" ]] && log_info "bowtie1 index directory found: ${D_BOWTIE_INDEX}" \
                             || log_fatal "bowtie1 index directory not found: ${D_BOWTIE_INDEX}"
[[ -d "${D_GSNAP_INDEX}" ]] && log_info "gsnap index directory found: ${D_GSNAP_INDEX}" \
                            || log_fatal "gsnap index directory not found: ${D_GSNAP_INDEX}"

# add config settings to makefile
sed 's/=/ := /' ${pipe_dir}/lib/config.sh > ${run_dir}/config.mk
echo "D_PIPE := $(basename ${pipe_dir})" >> ${run_dir}/config.mk
echo "D_BIN  := $(basename ${pipe_dir})/bin" >> ${run_dir}/config.mk
echo "D_RUN  := ${run_dir}" >> ${run_dir}/config.mk

# copy rump makefile
cp ${pipe_dir}/data/makefile.in makefile

# create sample specific parts of makefile
${pipe_dir}/bin/make_sample_mk ${1} > samples.mk