#! /bin/bash
SCRIPT=$(basename $0)
USAGE="
NAME
    ${SCRIPT} - create splice site file for gsnap alignments

SYNOPSIS
    ${SCRIPT} gtf_transcripts out_file

ARGUMENTS
    gtf_transcripts
        gtf format file of transcripts to be used as source of
        known splice sites
    out_file
        output file

DESCRIPTION
    uses gsnap/gmap tools to create .iit file for alignment
    of RNASeq data.
"

function usage {
    echo "${USAGE}" >&2
}
pipe_dir=$( cd $(dirname $0)/.. && pwd)
source ${pipe_dir}/lib/common.sh

if [[ -z "$1" || -z "$2" ]]; then
    usage
    exit ${EX_FAIL}
fi

[[ -f "$1" ]] && log_info "GTF file found: ${1}" \
              || log_fatal "GTF file not found: ${1}"

cat ${1} | gtf_splicesites | iit_store -o ${2}
