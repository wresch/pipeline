################################################################################
# settings
################################################################################
set -o pipefail

################################################################################
# exit codes
################################################################################
# keep it simple and stick to good error messages instead of complicated exit codes
EX_OK=0
EX_FAIL=1

################################################################################
# logging
################################################################################
LOG_LEVEL=${LOG_LEVEL:-DEBUG}

function log_set_level() {
    LOG_LEVEL=${1:-DEBUG}
    case $LOG_LEVEL in
     WARN)  exec 3>&2         4> /dev/null 5> /dev/null;;
     INFO)  exec 3>&2         4>&2         5> /dev/null;;
     DEBUG) exec 3>&2         4>&2         5>&2;;
     *) exec 3> /dev/null 4> /dev/null 5> /dev/null;;
    esac
}
log_set_level


function log_error() {
    if [ "$1" = "-n" ]; 
    then 
        echo "" >&3
        shift
    fi
    echo -e "ERROR|$( date +%r )|$@" >&3
}

function log_fatal() {
    if [ "$1" = "-n" ]; 
    then 
        echo "" >&3
        shift
    fi
    echo -e "FATAL|$( date +%r )|$@" >&3
    exit ${EX_FAIL}
}
    

function log_warn() {
    if [ "$1" = "-n" ]; 
    then 
        echo "" >&3
        shift
    fi
    echo -e "WARN |$( date +%r )|$@" >&3
}

function log_info() {
    if [ "$1" = "-n" ]; 
    then 
        echo "" >&4
        shift
    fi
    echo -e "INFO |$( date +%r )|$@" >&4
}


function log_debug() {
    if [ "$1" = "-n" ]; 
    then 
        echo "" >&5
        shift
    fi
    echo -e "DEBUG|$( date +%r )|$@" >&5
}

################################################################################
# UTILITIES
################################################################################

function cat_file() {
    local f=${1}
    [[ ! -z "${f}" ]] || log_fatal "cat_file requires 1 argument (compressed or uncompressed file)"
    [[ -f "${fq}" ]]  || log_fatal "fastq file not found: ${fq}"
    # strip the gz if it's there
    fngz=${f%.gz}
    if [[ -f ${fngz} ]]
    then
	cat ${fngz}
    else
	gunzip -c ${f}
    fi
}

function fastq_score_type() {
    # NOTE: assumes that there are no line breaks allowed!
    local fq=${1}
    cat_file ${fq} \
	| head -n 20000 \
	| awk 'NR % 4 == 0' \
	| tr -d '\n' \
	| od -A n -t u1 -v \
	| awk 'BEGIN {min = 256; max = 0}
               {for (i=1; i <= NF; i++) {if ($i > max) max = $i; if ($i < min) min = $i}}
               END {if (max <= 75 && min < 59) print "phred33";
                    else if (max > 73 && min >= 64) print "phred64";
                    else if (min >= 59 && min < 64 && max > 73) print "solexa";
                    else print "unknown";}'

}