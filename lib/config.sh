D_SEQ=seq_files
D_ALN=aln_files
D_QC=qc/fastqc
D_BATCH=batch_files
D_TRACK=track_files
D_OTHER=other_files
D_ANNOT=annot_files
D_TEMP=temp_files


CONTAMINANTS=/data/Casellas/common/contaminants/contaminant_list.txt

D_BOWTIE_INDEX=/fdb/bowtie/indexes/
D_GSNAP_INDEX=/fdb/gmap/